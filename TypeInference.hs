module TypeInference (TypingJudgment, Error(..), inferType)

where

import List(intersect)
import Exp
import Type
import Unification

type TypingJudgment = (Env, AnnotExp, Type)
data Error a = OK a | Error String

inferType :: PlainExp -> Error TypingJudgment
inferType e = case infer' e 0 of
    OK (_, tj) -> OK tj
    Error s -> Error s

infer' :: PlainExp -> Int -> Error (Int, TypingJudgment)
infer' expr n = case expr of
    VarExp x  -> OK (n+1, (extendE emptyEnv x (TVar n), VarExp x, TVar n))
    ZeroExp   -> OK (n,   (emptyEnv, ZeroExp, TNat))
    TrueExp   -> OK (n,   (emptyEnv, TrueExp, TBool))
    FalseExp  -> OK (n,   (emptyEnv, FalseExp, TBool))
    SuccExp x -> case infer' x n of 
            OK (n', (env, aExp, tExp)) ->
                case mgu [(tExp, TNat)] of
                    UOK s       -> OK (n', (s <.> env, s <.> (SuccExp aExp), TNat))
                    UError t1 _ -> Error ("el argumento no es natural: " ++ (show t1))
            Error s -> Error s
    PredExp x -> case infer' x n of 
            OK (n', (env, aExp, tExp)) ->
                case mgu [(tExp, TNat)] of
                    UOK s       -> OK (n', (s <.> env, s <.> (PredExp aExp), TNat))
                    UError t1 _ -> Error ("el argumento no es natural: " ++ (show t1))
            Error s -> Error s
    IsZeroExp x -> case infer' x n of 
            OK (n', (env, aExp, tExp)) ->
                case mgu [(tExp, TNat)] of
                    UOK s       -> OK (n', (s <.> env, s <.> (IsZeroExp aExp), TBool))
                    UError t1 _ -> Error ("el argumento no es natural: " ++ (show t1))
            Error s -> Error s
    AppExp x y -> case infer' x n of
            OK (nx, (envx, annx, tx)) ->
                case infer' y nx of
                    OK (ny, (envy, anny, ty)) ->
                        let tRes = TVar ny in 
                        let eRes = joinE [envx, envy] in
                        case mgu ((tx, TFun ty tRes):(getGoals envx envy)) of
                            UOK s        -> OK (ny+1, (s <.> eRes, s <.> (AppExp annx anny), tRes))
                            UError t1 t2 -> Error ("no es una función " ++ (show t1) ++ 
                                                   " o no toma como parámetro " ++ (show ty))
                    Error s -> Error s
            Error s -> Error s
                
    _ -> Error "no implementado"

getGoals :: Env -> Env -> [UnifGoal]
getGoals env1 env2 = [(evalE env1 x, evalE env2 x) |
                        x <- intersect (domainE env1) (domainE env2)]

uError :: Type -> Type -> Error (Int, a)
uError t1 t2 = Error $ "Cannot unify " ++ show t1 ++ " and " ++ show t2
