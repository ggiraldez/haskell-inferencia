module Examples(expr)
where

import Text.PrettyPrint
import ParserLC
import PrettyPrintLC
import TypeInference

testAll :: Doc
testAll= cat (map test definedExprs)

test :: Int -> Doc
test n = pprintTuple (expr n, inferExpr (expr n))

inferExpr :: String -> Doc
inferExpr s = ppTypingResult (inferType (parseLC s))

pprintTuple :: (String, Doc) -> Doc
pprintTuple (origExpr, annotExpr) = (text "W(") <> text origExpr <> (text ") = ") <> annotExpr

exprs= cat (map text (map (\n->show n ++ ") " ++ expr n)  definedExprs))

definedExprs :: [Int]
definedExprs= [1..29]

expr :: Int -> String
-- Ejemplos con Var, Zero, Succ y App
expr 1 = "x"
expr 2 = "0"
expr 3 = "succ(x)"
expr 4 = "succ(0)"
expr 5 = "f x"
expr 6 = "f 0"
expr 7 = "succ(f 0)"
expr 8 = "0 f"
expr 9 = "f 0 succ(succ(0))"
expr 10 = "f (0 succ(succ(0)))"
expr 11 = "f x y z"
expr 12 = "f (x y z)"
expr 13 = "f succ(x) y z"
expr 14 = "f (succ(x) y z)"
expr 15 = "x x"
-- Ejemplos con todos los constructores
expr 16 = "if true then succ(x y) else x (succ(y))"
expr 17 = "if true then x succ(succ(0)) else x true"
expr 18 = "(\\f -> if true then f 0 else f false) (\\x -> 0)"
expr 19 = "(if true then (\\x -> x) 0 else (\\x -> x) false)"
expr 20 = "(if true then (\\x -> 0) 0 else (\\x -> 0) false)"
expr 21 = "(\\x -> x)"
expr 22 = "\\s -> \\x -> \\y -> if isZero(x) then y else succ(s pred(x) y)"
expr 23 = "\\x -> \\y -> if x then y x else pred(succ(0))"
expr 24 = "if true then 0 else succ(0)"
expr 25 = "if true then x else 0"
expr 26 = "if x then x else 0"
expr 27 = "if x then y else 0"
expr 28 = "if x then y else z"
expr 29 = "if x then y else succ(y)"
expr n = error $ "La expresion " ++ show n ++ " no esta definida"
